﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Forms;

namespace FNR
{
    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if (parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if (parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
    class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            FileNames = new ObservableCollection<string>();
            _mask = "*.*";
        }
        public ObservableCollection<string> FileNames { get; set; }
        FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
        private string _mask;
        private string _selectedName;
        private int _countTrue;
        private int _countAll;
        private int _countChange;
        private string _search;
        private string _pathFolder;
        private int _pbSearchProgress;
        public int SaveCount;
        public int[] charCountTrue;
        public string SaveMask;
        public string SavePath;


        private ICommand _opd;
        private ICommand _searchText;
        private ICommand _changeText;
        private ICommand _deleteFile;

        public string Mask
        {
            get { return _mask; }
            set { _mask = value; OnPropertyChanged(nameof(Mask)); }
        }
        public string SelectedName
        {
            get { return _selectedName; }
            set { _selectedName = value; OnPropertyChanged(nameof(SelectedName)); }
        }
        public int CountTrue
        {
            get { return _countTrue; }
            set { _countTrue = value; OnPropertyChanged(nameof(CountTrue)); }
        }
        public int CountAll
        {
            get { return _countAll; }
            set { _countAll = value; OnPropertyChanged(nameof(CountAll)); }
        }
        public int CountChange
        {
            get { return _countChange; }
            set { _countChange = value; OnPropertyChanged(nameof(CountChange)); }
        }
        public string Search
        {
            get { return _search; }
            set { _search = value; OnPropertyChanged(nameof(Search)); }
        }
        public string DoWorkProgressChanged
        {
            get { return _search; }
            set { _search = value; OnPropertyChanged(nameof(DoWorkProgressChanged)); }
        }
        public int pbSearchProgress
        {
            get { return _pbSearchProgress; }
            set { _pbSearchProgress = value; OnPropertyChanged(nameof(pbSearchProgress)); }
        }
        public string PathFolder
        {
            get { return _pathFolder; }
            set { _pathFolder = value; OnPropertyChanged(nameof(PathFolder)); }
        }
        //----------------- OPD ------------
        public ICommand OPD
        {
            get  { return _opd ?? (_opd = new RelayCommand(OPDExecute,OPDCanExecute)); }
        }
        private void OPDExecute()
        {
            FileNames.Clear();
            string[] arrNames = null;
            var FBDresult = folderBrowserDialog.ShowDialog();
            if (FBDresult == DialogResult.OK)
            {
                PathFolder = folderBrowserDialog.SelectedPath;
                if (!string.IsNullOrEmpty(Mask))
                {
                    SaveMask = Mask;
                    arrNames = Directory.GetFiles(folderBrowserDialog.SelectedPath, Mask);
                }
                else
                {
                    arrNames = Directory.GetFiles(folderBrowserDialog.SelectedPath);
                }
                foreach (string filename in arrNames)
                {
                    FileNames.Add(filename);
                    CountAll++;
                }
            }
        }
        private bool OPDCanExecute()
        {
            return true;
        }       
        //----------------- SearchText ------------
        public ICommand SearchText
        {
            get { return _searchText ?? (_searchText = new RelayCommand<string>(SearchTextExecute, SearchTextCanExecute)); }
        }
        private void SearchTextExecute(string strSearch)
        {
            CountChange = 0;
            CountTrue = 0;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWorkSearch;
            worker.ProgressChanged += worker_ProgressChangedSearch;
            worker.RunWorkerCompleted += worker_RunWorkerCompletedSearch;
            worker.RunWorkerAsync(strSearch);
        }
        private bool SearchTextCanExecute(string strSearch)
        {
            if (SaveMask != Mask && !string.IsNullOrEmpty(PathFolder))
            {
                FileNames.Clear();
                CountAll = 0;
                string[] arrNames = null;
                DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(PathFolder));
                if (dir.Exists)
                {
                    if (!string.IsNullOrEmpty(Mask))
                    {
                        SaveMask = Mask;
                        arrNames = Directory.GetFiles(PathFolder, Mask);
                    }
                    else
                    {
                        SaveMask = Mask;
                        arrNames = Directory.GetFiles(PathFolder);
                    }
                    foreach (string filename in arrNames)
                    {
                        FileNames.Add(filename);
                        CountAll++;
                    }
                }
            }

            if (PathFolder != SavePath && !string.IsNullOrEmpty(PathFolder))
            {
                FileNames.Clear();
                CountAll = 0;
                string[] arrNames = null;
                DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(PathFolder));
                if (dir.Exists)
                {                    
                    SavePath = PathFolder;
                    if (!string.IsNullOrEmpty(Mask))
                    {
                        SaveMask = Mask;
                        arrNames = Directory.GetFiles(PathFolder, Mask);
                    }
                    else
                        arrNames = Directory.GetFiles(PathFolder);
                    foreach (string filename in arrNames)
                    {
                        FileNames.Add(filename);
                        CountAll++;
                    }
                    return true;
                }
                else
                    return false;
            }
            if (!string.IsNullOrEmpty(strSearch) && FileNames.Count > 0 && !string.IsNullOrEmpty(PathFolder))
            {
                return true;
            }
            else
                return false;
        }

        //----------------- Worker Search---------
        void worker_DoWorkSearch(object sender, DoWorkEventArgs e)
        {
            string strSearch = (string)e.Argument;
            char[] charArrStrSearch = strSearch.ToCharArray();
            int countSymbols = 0;
            int progress = 0;
            int num = -1;
            charCountTrue = new int[CountAll];
            foreach (string filename in FileNames)
            {                
                num++;
                string textOfFile = File.ReadAllText(filename);
                char[] charArrTextOfFile = textOfFile.ToCharArray();
                for (int i = 0; i < charArrTextOfFile.Length; i++)
                {
                    for (int j = 0; j < charArrStrSearch.Length; j++)
                    {
                        if (charArrTextOfFile[i] == charArrStrSearch[j])
                        {                            
                            countSymbols++;	

                            if (countSymbols == charArrStrSearch.Length)
                            {
                                i--;
                                CountTrue++;
                                charCountTrue[num]++;
                                countSymbols = 0;
                            }
                            if (i < charArrTextOfFile.Length - 1)
                                i++;
                            else
                                break;
                        }
                        else
                        {
                            j = 0;
                            countSymbols = 0;
                            break;
                        }                            
                    }
                }
                Thread.Sleep(300);
                progress += 100 / FileNames.Count;
                if (progress > 90 && progress < 100)
                    progress = 100;
                (sender as BackgroundWorker).ReportProgress(progress);
            }            
            e.Result = progress;
        }
        void worker_ProgressChangedSearch(object sender, ProgressChangedEventArgs e)
        {
            pbSearchProgress = e.ProgressPercentage;
        }
        void worker_RunWorkerCompletedSearch(object sender, RunWorkerCompletedEventArgs e)
        {
            if (CountTrue == 0)
                System.Windows.Forms.MessageBox.Show("Совпадений нет!");
            System.Windows.Forms.MessageBox.Show("Поиск завершен  " + $"{e.Result}%");
        }
        //----------------- ChangeText ------------
        public ICommand ChangeText
        {
            get { return _changeText ?? (_changeText = new RelayCommand<string>(ChangeTextExecute, ChangeTextCanExecute)); }
        }
        private void ChangeTextExecute(string strChange)
        {
            CountChange = 0;
            SaveCount = CountTrue;
            CountTrue = 0;
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWorkChange;
            worker.ProgressChanged += worker_ProgressChangedChange;
            worker.RunWorkerCompleted += worker_RunWorkerCompletedChange;
            worker.RunWorkerAsync(strChange);
        }
        private bool ChangeTextCanExecute(string strChange)
        {
            if (!string.IsNullOrEmpty(strChange) && !string.IsNullOrEmpty(Search) && FileNames.Count > 0 && CountTrue != 0)
                return true;
            else
                return false;
        }
        //----------------- Worker Change---------
        void worker_DoWorkChange(object sender, DoWorkEventArgs e)
        {
            string strChange = (string)e.Argument;           
            char[] charArrStrSearch = Search.ToCharArray();
            char[] charArrStrChange = strChange.ToCharArray();
            int countSymbols = 0;
            int countForCharArrStrChange = 0;
            int num = -1;
            string textOfFile;
            int newCount = 0;
            int progress = 0;
            foreach (string filename in FileNames)
            {
                num++;
                char[] newCharArr;
                newCount = (strChange.Length - Search.Length) * charCountTrue[num];
                textOfFile = File.ReadAllText(filename);
                char[] charArrTextOfFile = textOfFile.ToCharArray();
                if (newCount > 0)
                    newCharArr = new char[charArrTextOfFile.Length + newCount];
                else
                    newCharArr = new char[charArrTextOfFile.Length];
                for (int i = 0; i < charArrTextOfFile.Length; i++)
                {
                    for (int j = 0; j < charArrStrSearch.Length; j++)
                    {
                        newCharArr[countForCharArrStrChange++] = charArrTextOfFile[i];
                        if (charArrTextOfFile[i] == charArrStrSearch[j])
                        {
                            countSymbols++;
                            if (countSymbols == charArrStrSearch.Length)
                            {
                                countForCharArrStrChange -= Search.Length;
                                for (int k = 0; k < strChange.Length; k++)
                                {
                                    newCharArr[countForCharArrStrChange++] = charArrStrChange[k];                                    
                                }
                                i--;
                                CountChange++;
                                countSymbols = 0;
                            }
                            if (i < charArrTextOfFile.Length - 1)
                                i++;
                            else
                                break;
                        }
                        else
                        {
                            j = 0;
                            countSymbols = 0;
                            break;
                        }
                    }
                }
                char[] tmp = new char[textOfFile.Length + newCount];
                for (int i = 0; i < textOfFile.Length + newCount; i++)
                {
                    tmp[i] = newCharArr[i];
                }
                string tempParam = new string(tmp);
                progress += 100 / FileNames.Count;
                if (progress > 90 && progress < 100)
                    progress = 100;
                (sender as BackgroundWorker).ReportProgress(progress);
                Thread.Sleep(300);
                File.WriteAllText(filename, tempParam);
                countForCharArrStrChange = 0;
            }
            e.Result = progress;
        }
        void worker_ProgressChangedChange(object sender, ProgressChangedEventArgs e)
        {
            pbSearchProgress = e.ProgressPercentage;
        }
        void worker_RunWorkerCompletedChange(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Windows.Forms.MessageBox.Show("Замена завершена  " + $"{e.Result}%");
        }
        //----------------- DeleteFile ------------
        public ICommand DeleteFile
        {
            get { return _deleteFile ?? (_deleteFile = new RelayCommand<string>(DeleteFileExecute, DeleteFileCanExecute)); }
        }
        private void DeleteFileExecute(string fileNames)
        {
            FileNames.Remove(fileNames);
            CountAll--;
            CountTrue = 0;
            CountChange = 0;
        }
        private bool DeleteFileCanExecute(string fileNames)
        {
            return true;
        }
        //--------------------------------------------------------------------
        private void OnPropertyChanged(string PropertyName)
        {
            if (PropertyName == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
